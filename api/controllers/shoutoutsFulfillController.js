const shoutoutsFulfill = require('../models/shoutoutsFulfill');

const shoutoutsFulfillController = () => {
    const getAll = async (req, res) => {
        try {
            const data = await shoutoutsFulfill.findAll();
            return res.status(200).json(data);
        } catch (err) {
            console.log(err);
            return res.status(500).json({ msg: 'Internal server error' });
        }
    };

    const addShoutoutsFulfill = async (req, res) => {
        try {
            const data = await shoutoutsFulfill.create(req.body)
            return res.status(200).json(data);
        } catch(err) {
            console.log(err)
            return res.status(500).json({ msg: 'Internal server error' });
        }
    }
    
    return {
        getAll,
        addShoutoutsFulfill,
    };
};

module.exports = shoutoutsFulfillController;
