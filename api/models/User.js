const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const hooks = {
  beforeCreate(user) {
    user.password = bcryptService().password(user); // eslint-disable-line no-param-reassign
  },
};

const User = sequelize.define('User', {
  email: {
    type: Sequelize.STRING,
    unique: true,
  }, 
  password: {
    type: Sequelize.STRING, 
  },
  userType: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  firstName: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  }, 
  lastName: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  emailVerified: {
    type: Sequelize.BOOLEAN,
    defaultValue: false,
  },
  profilePhoto: {
    type: Sequelize.STRING,
  },
}, { hooks });

// eslint-disable-next-line
User.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  delete values.password;
  return values;
};

module.exports = User;
