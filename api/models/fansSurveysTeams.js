const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const fans_surveys_teams = sequelize.define('fans_surveys_teams', {
  userid: {
    type: Sequelize.INTEGER,
  },
  teamID: {
    type: Sequelize.INTEGER,
  },
  date_taken: {
    type: Sequelize.DATE,
  },
});

// eslint-disable-next-line
fans_surveys_teams.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = fans_surveys_teams;
