const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');


const listingCD = sequelize.define('listing_conference_division', {
  sportID: {
    type: Sequelize.STRING, 
  },
  name: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  }, 
});

// eslint-disable-next-line
listingCD.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = listingCD;
