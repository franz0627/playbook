const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const listing_sports = sequelize.define('listing_sports', {
  name: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  }, 
});

// eslint-disable-next-line
listing_sports.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = listing_sports;
