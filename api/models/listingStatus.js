const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const listingStatus = sequelize.define('listing_status', {
  description: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  }, 
});

// eslint-disable-next-line
listingStatus.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = listingStatus;
