const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const listing_teams = sequelize.define('listing_teams', {
  confdivID: {
    type: Sequelize.INTEGER,
  }, 
  name: {
    type: Sequelize.STRING, 
    isAlphanumeric: true,
  }, 
}); 

// eslint-disable-next-line
listing_teams.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = listing_teams;
