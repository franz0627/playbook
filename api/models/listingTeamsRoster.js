const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');


const listingTeamsRoster = sequelize.define('listing_teams_roster', {
  athleteID: {
    type: Sequelize.INTEGER,
  }, 
  teamID: {
    type: Sequelize.INTEGER, 
  },
 
});

// eslint-disable-next-line
listingTeamsRoster.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = listingTeamsRoster;
