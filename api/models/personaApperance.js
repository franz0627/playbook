const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const personalApperance = sequelize.define('personal_apperance', {
  userID: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  location: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  date_created: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  date_appearance: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  time_start: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  time_end: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  athleteID: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  description: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  transID: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  status: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },

});

// eslint-disable-next-line
personalApperance.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = personalApperance;
