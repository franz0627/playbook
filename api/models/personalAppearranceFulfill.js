const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');


const personalA = sequelize.define('personal_appearrance_fulfill', {
  pappearID: {
    type: Sequelize.STRING,
  }, 
  location: {
    type: Sequelize.STRING, 
    isAlphanumeric: true,
  },
  Geotag: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  calendar_url: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  }, 
  status: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  feedback: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  }, 
  price: {
    type: Sequelize.DOUBLE,
  },
});

// eslint-disable-next-line
personalA.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = personalA;
