const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');


const purchaseHistory = sequelize.define('purchase_history', {

  userID: {
    type: Sequelize.INTEGER,
  },
  type: {
    type: Sequelize.INTEGER,
  },
  typesID: {
    type: Sequelize.INTEGER,
  },
  status: {
    type: Sequelize.INTEGER,
  },
  payment_info: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },

});

// eslint-disable-next-line
purchaseHistory.prototype.toJSON = function () {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = purchaseHistory;
