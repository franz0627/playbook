const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const shoutouts = sequelize.define('shoutouts', {
  userID: {
    type: Sequelize.INTEGER,
    isAlphanumeric: true,
  },
  date_created: {
    type: Sequelize.DATE, 
    isAlphanumeric: true,
  },
  name1: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  name2: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  special_occ: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  special_message: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  received_via: {
    type: Sequelize.INTEGER,
    isAlphanumeric: true,
  },
  email: {
    type: Sequelize.STRING,
    unique: true,
  },
  phone: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  athleteID: {
    type: Sequelize.INTEGER,
    isAlphanumeric: true,
  },
  transID: {
    type: Sequelize.INTEGER,
    isAlphanumeric: true,
  },
  date_fulfill: {
    type: Sequelize.DATE,
    isAlphanumeric: true,
  },
  status: {
    type: Sequelize.INTEGER,
    isAlphanumeric: true,
  },
}); 
 
// eslint-disable-next-line
shoutouts.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};

module.exports = shoutouts;
