const Sequelize = require('sequelize');
const sequelize = require('../../config/database');

const shoutOuts = sequelize.define('shoutouts_fulfill', {
  shoutoutID: {
    type: Sequelize.INTEGER,
  },
  date_fulfill: {
    type: Sequelize.DATE,
    defaultValue: false,
  },
  video: { 
    type: Sequelize.STRING,
  },
});

shoutOuts.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};
 

module.exports = shoutOuts;
