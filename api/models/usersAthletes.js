const Sequelize = require('sequelize');
const bcryptService = require('../services/bcrypt.service');

const sequelize = require('../../config/database');

const users_athletes = sequelize.define('users_athletes', {
  fname: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  lname: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  profilepic: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  moreinfo: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  }, 
  services: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  charities: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  payment: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },

});

// eslint-disable-next-line
users_athletes.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  // delete values.password;
  return values;
};

module.exports = users_athletes;
