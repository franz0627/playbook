const Sequelize = require('sequelize');

const sequelize = require('../../config/database');

const userFans = sequelize.define('users_fans', {
  email: {
    type: Sequelize.STRING,
    unique: true,  
  },
  firstName: { 
    type: Sequelize.STRING,
  },
  surName: {
    type: Sequelize.STRING,
    isAlphanumeric: true,
  },
  email_verified: { 
    type: Sequelize.INTEGER,
  }, 
  date_created: {
    type: Sequelize.DATE,
  },
  profilepic: {
    type: Sequelize.STRING,
  },
});

userFans.prototype.toJSON = function() {
  const values = Object.assign({}, this.get());
  return values;
};
 

module.exports = userFans;
