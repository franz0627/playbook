const shoutoutsFulfillController = require("../../api/controllers/shoutoutsFulfillController");

const privateRoutes = {
  'GET /users': 'UserController.getAll',
  'GET /getAllShoutoutsFulfill': 'shoutoutsFulfillController.getAll',
};

module.exports = privateRoutes;
