const UserController = require("../../api/controllers/UserController");
const shoutoutsFulfillController = require("../../api/controllers/shoutoutsFulfillController");

const publicRoutes = {
  'POST /register': 'UserController.register',
  'POST /login': 'UserController.login',
  'POST /validate': 'UserController.validate',
  'POST /addShoutoutsFulfill': 'shoutoutsFulfillController.addShoutoutsFulfill',
};

module.exports = publicRoutes;
     